/*
 * @Author: crazy_system 909395091@qq.com
 * @Date: 2023-12-29 16:37:59
 * @LastEditors: crazy_system 909395091@qq.com
 * @LastEditTime: 2023-12-29 18:00:10
 * @FilePath: /WorkSpace/CplusplusBasis/Basis/CopyFile.cpp
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
#include<string>
#include<fstream>
#include<iostream>
using namespace std;
static const int bufferLength = 2048;
bool CopyFile(const string& src,const string& dst)
{
    //打开源文件和目标文件
    //源文件以二进制读方式打开
    ifstream in(src.c_str(),std::ios::in|std::ios::binary);
    //目标文件以二进制写的方式打开
    ofstream out(dst.c_str(),std::ios::out|std::ios::binary|std::ios::trunc);
    //判断文件打开是否成功，失败返回false
    if(!in||!out)
        return false;
    //从源文件中读取数据，写到目标文件中
    //通过读取源文件EOF来判断读写是否结束
    char temp[bufferLength];
    while(!in.eof())
    {
        in.read(temp,bufferLength);
        streamsize count = in.gcount();
        out.write(temp,count);
    }
    //关闭源文件和目标文件
    in.close();
    out.close();
    return true;
}
int main()
{
    cout<<CopyFile("1.mp4","1-1.mp4")<<endl;
    return 0;
}