#include<iostream>
#include<vector>
#include<bitset>
using namespace std;

double averageOne(vector<int>& v){
    double ret = 0.0;
    vector<int>::iterator it = v.begin();//auto it = v.begin()
    for(;it!=v.end();++it){
        ret+=*it;
    }
    return ret/v.size();
}
double averageTwo(vector<vector<int>>& v){
    double ret = 0.0;
    unsigned int size = 0;
    for(unsigned i = 0;i<v.size();++i)
        for(unsigned j = 0;j<v[i].size();++j){
            ret += v[i][j];
            size++;
        }
    return ret/size;
}
void bits(){
    bitset<10> ptiv = 0xFF;
    bitset<10> P_BACKUP = (1<<6);
    bitset<10> P_ADMIN = (1<<9);
    cout<<ptiv<<endl;
    cout<<P_BACKUP<<endl;
    cout<<P_ADMIN<<endl;
}
int main(){
    int A = 10,B = 20;
    int C = A<<2;
    // cout<<C<<endl;
    // C>>=1;
    // cout<<"A|B:"<<(A|B)<<endl;
    // cout<<"~A:"<<(~A)<<endl;
    // cout<<&A<<endl;
    // int *a = &A;
    // cout<< a <<endl;
    // cout<< *a <<endl;

    // char c = 'yes';
    // cout<<c<<endl;

    bits();


    return 0;
}