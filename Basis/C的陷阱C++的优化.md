<!--
 * @Author: crazy_system 909395091@qq.com
 * @Date: 2023-12-26 21:48:34
 * @LastEditors: crazy_system 909395091@qq.com
 * @LastEditTime: 2023-12-27 15:00:21
 * @FilePath: /WorkSpace/CplusplusBasis/Basis/C的陷阱C++的优化.md
-->
# C语言陷阱

## C语言字符语法陷阱

char类型与char*类型

```
char a = 'asd';//正确，但是会进行截取
char b = "asd";//错误
const char* a = "a";//正确
const char* b = 'a';//错误
```

C++的解决
引入string类型

## C语言数组退化

在C语言中将一个数组传递到其他函数中的时候，这个数组的大小会变成数组中的一个元素，发生了数组退化现象（数组做参数退化）

C++的解决
STL容器以及引用的使用，实现底层包装，保证效率而且安全简单 

## C语言移位运算

在C语言的位移操作，没有指明是算术位移还是逻辑位移
建议，在进行位移的操作的时候转化为无符号数

移位操作有位数的限制（>0&&<位数）

C++的解决方案
bitset的使用

## C语言强制类型转化

C语言中的隐含着类型的转换，产生的Bug会难以发现

C++的解决
static_case,const_cast,dynamic_cast,reinterpret_cast

## C语言整数溢出

C++的解决
扩展库的使用，如：boost

## C语言字符串

使用'\0'作为结束符

C++的解决
C++引入了string类

# 字符指针与字符数组

char* c = "HelloWorld";
char c[11] = {"HelloWorld"};

```C++
strlen
strcmp//根据ASCII数值
strncpy(s1,s2,n)//将s2前n个拷贝到s1里面
strcat//后面借到前面
strchr//字符串第一次出现字符的位置
strstr//后面字符第一次出现在前面字符的位置
```
