<!--
 * @Author: crazy_system 909395091@qq.com
 * @Date: 2023-12-28 20:45:34
 * @LastEditors: crazy_system 909395091@qq.com
 * @LastEditTime: 2024-01-02 11:27:55
 * @FilePath: /WorkSpace/CplusplusBasis/Basis/C++基础.md
-->
# C++基础

## 自定义结构体

结构体数据对其--缺省对齐原则

```C++
//32位CPU
char //任何地址
short//偶数地址
int//4的整数倍
double//8的整数倍
```

修改默认编译选项
```C++
// visual C++:
    #pragma pack(n)
// g++
    __attribute__(aliged(n))
    __attribute__(__packed__)
```

## overload

函数名称相同，但是参数不相同

## 函数指针与返回指针的函数

每一个函数都占用一段内存单元，他们有一个起始地址，指向函数入口地址的指针成为函数指针
`一般形式：数据类型(*指针变量名)(参数表) 例子：int(*p)(int)`

注意与返回指针的函数之间的区别：
int(*p)(int);//是指针，指向一个函数入口地址
int* p(int);//是函数，返回的值是一个指针

## 命名空间

命名空间作为附加信息来区分不同库中相同名称的函数、类、变量等
本质上，命名空间就是定义了一个范围
`关键字：using、namespace`

## inline

## 递归

递归四个基本法则

1. 基准情形：无须递归就能解出
2. 不断推进：每一次递归调用都必须使求解状况接近基准情形的方向推进
3. 设计法则：假设所有的递归调用都能运行
4. 合成效益法则：求解一个问题的同一个实例时，切勿在不同的递归调用中做重复性工作

递归是一种重要的编程思想：

1. 许多算法都使用递归思想
2. 递归缺陷
   1. 空间上需要大量栈空间
   2. 时间上可能需要有大量重复运算

递归优化：

1. 尾递归：所有递归形式的调用都出现在函数的末尾
2. 使用循环替代
3. 使用动态规划，空间换时间

# C++高级语法

## I/O蓅

- 传统C语言I/O有printf、sacnf、getch、gets等函数
  - 不可编程，仅仅识别固有类型
  - 移植性差
- C++中的I/O蓅istream、ostream等
  - 可编程
  - 简化编程

`cin.ignore(numeric_limits<std::streamsize>::max(),'\n')`清空缓存区

## 文件操作

1. 打开文件用于读和写`open`
2. 检查打开是否成功`fail`
3. 读或者写`read，write`
4. 检查是否读完`EOF（end of file）`
5. 使用完文件后关闭文件`close`

|文件打开方式|方式|
|:---:|:---:|
|ios::in|打开文件进行读操作（ifstream）|
|ios::out|打开文件进行写操作（ofstream）|
|ios::ate|打开一个已有输入或输出文件并查找到文件尾|
|ios::app|打开文件以便在文件的尾部添加数据|
|ios::nocreate|如果文件不存在，则打开失败|
|ios::trunc|如果文件存在，清除文件原有内容|
|ios::binary|以二进制方式打开|

```C++
bool CopyFile(const string& src,const string& dst)
{
    //打开源文件和目标文件
    //源文件以二进制读方式打开
    ifstream in(src.c_str(),std::ios::in|std::ios::binary);
    //目标文件以二进制写的方式打开
    ofstream out(dst.c_str(),std::ios::out|std::ios::binary|std::ios::trunc);
    //判断文件打开是否成功，失败返回false
    if(!in||!out)
        return false;
    //从源文件中读取数据，写到目标文件中
    //通过读取源文件EOF来判断读写是否结束
    char temp[bufferLength];
    while(!in.eof())
    {
        in.read(temp,bufferLength);
        streamsize count = in.gcount();
        out.write(temp,count);
    }
    //关闭源文件和目标文件
    in.close();
    out.close();
    return true;
}
```

## 操作符重载

```h
#pragma once
#include<iostream>
using namespace std;
class complex
{
public:
	complex();
	complex(double r,double i);
	complex(const complex& c);
	virtual ~complex();
	double getReal() const;
	void setReal(double d);
	double getImage() const;
	void setImage(double i);
	complex operator+(const complex& c);
	complex& operator=(const complex& c);
	complex& operator++();//前置++
	complex operator++(int);//后置++
	friend ostream& operator<<(ostream& os,const complex& c);
	friend istream& operator>>(istream& is, complex& c);
private:
	double _real;
	double _image;
};
```

```C++
#include "complex.h"

complex::complex() 
{
	_real = 0.0;
	_image = 0.0;
}

complex::complex(double r, double i)
{
	_real = r;
	_image = i;
	cout << "complex::complex" << endl;
}

complex::complex(const complex& c) 
{
	_real = c._real;
	_image = c._image;
}

double complex::getReal() const 
{
	return _real;
}

void complex::setReal(double d) 
{
	_real = d;
}

double complex::getImage() const 
{
	return _image;
}

void complex::setImage(double i) 
{
	_image = i;
}

complex complex::operator+(const complex& c) 
{
	complex temp(_real+c._real,_image+c._image);
	return temp;
}

complex& complex::operator=(const complex& c) 
{
	if (this != &c) {
		_real = c._real;
		_image = c._image;
	}
	return *this;
}

complex& complex::operator++()
{
	_real++;
	_image++;
	return *this;
}

complex complex::operator++(int)
{
	complex temp(*this);
	_real++;
	_image++;
	return temp;
}

complex::~complex()
{
	cout << "complex::~complex" << endl;
}

ostream& operator<<(ostream& os, const complex& c)
{
	os << "real: " << c._real << " image: " << c._image;
	return os;
}

istream& operator>>(istream& is, complex& c)
{
 is >> c._real >> c._image;
 return is;
}
```

## 头文件重复包含

1. 使用宏

```C
#ifndef _XXX_H_
#define _XXX_H_
...
#endif
```

- 优点：可移植性好
- 缺点：无法防止宏名重复，难以排错

2. 使用编译器

```C++
#pragma one
```

- 优点：防止宏名重复，易排错
- 缺点：可移植性不好

## 拷贝

- 浅拷贝：只拷贝指针地址，C++默认拷贝构造函数与赋值运算符重载都是浅拷贝；节省空间，容易引发多次释放。
- 深拷贝：重新分配堆空间，拷贝指针指向内容；浪费空间，不会导致多次释放。

- 兼有两者的优点：
  - 引用计数
  - C++新标准的移动语义

## 面向对象三大特性

- 封装性：数据和代码捆绑在一起，避免外界干扰和不确定性访问，封装可以使得代码模块化
- 继承性：让某种类型对象获得另一个类型对象的属性和方法，继承可以扩展已存在代码
- 多态性：同一事物表现出不同事物的能力，即向不同对象会产生不同的行为，多态的目的则是为了接口重用

# C++编程思想

## void*，NULL和nullptr

- 在C语言中

```C
#define NULL ((void*)0)
```

- 在C++语言中

```C++
#ifndef NULL
  #ifdef __cplusplus
    #define NULL 0
  #else
    #define NULL ((void*)0)
  #endif
#endif
```

- 在C++11中，nullptr用来代替(void*)0，NULL则只是表示0

## 类型转化

- const_cast:用于转换指针或者引用，去掉类型的const属性
- reinterpret_cast（危险）：重新解释类型，既不检查指向内容，也不检查指针类型本身；但是要求转换前后所占内存大小一致，否则会引发编译错误
- static_cast：用于基本类型转换，有继承关系类对象和类指针之间转换，由程序员来确保转换安全，不会产生动态转换的类型安全检查开销
- dynamic_cast：只能用于含有虚函数的类，必须用在多态体系中，用于类层次见向上和向下的转化；向下转化时，如果时非法的对于指针返回NULL

## 泛型编程思想

- 不同于面向对象的动态期多态，泛型编程是一种静态期多态，通过编译器生成最直接代码
- 泛型编程可以将算法与特定类型，结构剥离，尽可能复用代码