/*
 * @Author: crazy_system 909395091@qq.com
 * @Date: 2024-01-02 16:12:49
 * @LastEditors: crazy_system 909395091@qq.com
 * @LastEditTime: 2024-01-02 16:12:52
 * @FilePath: /WorkSpace/CplusplusBasis/Basis/Thread.cpp
 */
#include<thread>
#include<mutex>
#include<iostream>
using namespace std;
mutex g_mutex;
void T1()
{
    g_mutex.lock();
    cout<<"T1 Hello"<<endl;
    g_mutex.unlock();
}
void T2(const char* str)
{
    g_mutex.lock();
    cout<<"T2 "<<str<<endl;
    g_mutex.unlock();
}
int main()
{
    thread t1(T1);
    t1.join();
    thread t2(T2,"Hello");
    t2.join();
    return 0;
}