/*
 * @Author: crazy_system 909395091@qq.com
 * @Date: 2024-01-02 11:36:31
 * @LastEditors: crazy_system 909395091@qq.com
 * @LastEditTime: 2024-01-02 11:36:34
 * @FilePath: /WorkSpace/CplusplusBasis/Basis/Sum.cpp
 */
#include<iostream>
using namespace std;
template<int n>
struct Sum
{
    enum Value{N = Sum<n-1>::N+n};
};
template<>
struct Sum<1>
{
    enum Value{N = 1};
};
int main()
{
    cout<<Sum<100>::N<<endl;
    return 0;
}

