<!--
 * @Author: crazy_system 909395091@qq.com
 * @Date: 2024-01-02 11:38:59
 * @LastEditors: crazy_system 909395091@qq.com
 * @LastEditTime: 2024-01-02 20:48:26
 * @FilePath: /WorkSpace/CplusplusBasis/Basis/C++进阶.md
-->
# C++进阶

## STL(Standard Template Library)

- STL算法是泛型的（generic），不与任何特定数据结构和对象绑定，不必在环境类似的情况下写代码
- STL算法可以量身定做，并且具有很高的效率
- STL可以进行扩充，你可以编写自己的组件并且能与STL标准组件进行配合
- 组件
  - 仿函数
  - 算法
  - 迭代器
  - 空间配置器
  - 容器
  - 适配器

## 容器

- 序列化容器
  - 其中元素可以排序，vector，list，deque等序列容器，而stack，queue，priority_queue则是适配器
- 关联式容器
  - 每个数据都是一个建和值组成，当被插入到容器时，按其建以某种特定规则放入适当位置；常见的是set，multiset，map，multimap

### map

```C++
struct DisPlay
{
    void operator()(pair<string,double> info)
    {
        cout<<info.first<<" : "<<info.second<<endl;
    }
};

int main()
{
    map<string,double> socres;
    //给map中增加数据
    socres["WangYi"] = 90.5;
    socres.insert( pair<string,double>("ChengEr",89.9) );
    socres.insert( map<string,double>::value_type("LiSi",83.2) );
    socres["ChengEr"] = 99.9;
    //便利map
    for_each(socres.begin(),socres.end(),DisPlay());
    //在map中查找元素
    map<string,double>::iterator it;
    it = socres.find("ChengEr");
    if(it!=socres.end())
    {
        cout<<it->first<<" socre is "<<it->second<<endl;
    }else{
        cout<<"Didn't find the key"<<endl;
    }
    //根据要求删除某种元素
    // it = socres.begin();
    // while(it != socres.end())
    // {
    //     if(abs(it->second-90.0)<0)
    //         socres.erase(it++);//注意迭代器失效问题
    //     else
    //         it++;
    // }

    for(it = socres.begin();it!=socres.end();it++)
    {
        if(it->second<90.0)
           it = socres.erase(it);
    }
    for_each(socres.begin(),socres.end(),DisPlay());
    return 0;
}

```

## 仿函数（functor）

- 仿函数一般不会单独使用，主要是搭配STL算法使用
- 函数指针不能满足STL对抽象性的要求，不能满足软件模块化要求，无法和STL其他搭配
- 本质就是类重载一个`operator()`,创建一个行为类似函数的对象


```C++
//C++方式
bool MySort(int a,int b)
{
    return a<b;
}
void Display(int a)
{
    cout<<a<<" ";
}
//C++泛型
template<class T>
inline bool MySortT(T const& a,T const& b)
{
    return a<b;
}
template<class T>
inline void DisplayT(T const& a)
{
    cout<<a<<" ";
}
//仿函数
struct SortF
{
    bool operator()(int a,int b)
    {
        return a<b;
    }
};
struct DisplayF
{
    void operator()(int a)
    {
        cout<<a<<" ";
    }
};
//C++仿函数模板
template<class T>
struct SortTF
{
    inline bool operator()(T const& a,T const& b)const
    {
        return a<b;
    }
};
template<class T>
struct DisplayTF
{
    inline void operator()(T const& a)const
    {
        cout<<a<<" ";
    }
};
int main()
{
    //C++
    int arr1[] = {12,532541,623,12152,12,896};
    sort(arr1,arr1+6,MySort);
    for_each(arr1,arr1+6,Display);
    cout<<endl;
    //C++泛型
    int arr2[] = {245,58,26452,34,26,265};
    sort(arr2,arr2+6,MySortT<int>);
    for_each(arr2,arr2+6,DisplayT<int>);
    cout<<endl;
    //C++仿函数
    int arr3[] = {1564,468,987,225,4541,36};
    sort(arr3,arr3+6,SortF());
    for_each(arr3,arr3+6,DisplayF());
    cout<<endl;
    //C++仿函数模板
    int arr4[] = {94,154,487256,48547,8748,4};
    sort(arr4,arr4+6,SortTF<int>());
    for_each(arr4,arr4+6,DisplayTF<int>());
    return 0;
}
```

## 算法

`<algorithm><numeric><functional>`

- 非可变序列算法：指不直接修改其所操作的容器的算法
- 可变序列算法：值可以修改它们所操作的容器内容算法
- 排序算法：包括对序列进行排序和合并的算法，搜索算法以及有序序列上的集合操作
- 数值算法：对容器内容进行数值计算

```C++
#include<iostream>
#include<algorithm>
#include<numeric>
#include<functional>
#include<vector>
using namespace std;
int main()
{
    int ones[] = {15,485,454,4852,458};
    int twos[] = {65489,594,98,456,20};
    int results[5];
    transform(ones,ones+5,twos,results,std::plus<int>());
    for_each(
        results,
        results+5,
        [](int a)->void{cout<<a<<" ";}
    );
    int arr[] = {154,5,12,186,3498,4987,2878,4,5,15,1,5};

    cout<<count(arr,arr+sizeof(arr)/sizeof(arr[0]),5)<<endl;

    cout<<count(arr,arr+sizeof(arr)/sizeof(arr[0]),bind2nd(less<int>(),10))<<endl;//<10

    cout<<count(arr,arr+sizeof(arr)/sizeof(arr[0]),bind1st(less<int>(),10))<<endl;//>10

    sort(arr,arr+sizeof(arr)/sizeof(arr[0]),[](int a,int b)->bool{return a<b;});

    for_each(arr,arr+sizeof(arr)/sizeof(arr[0]),[](int a)->void{cout<<a<<" ";});
    cout<<endl;

    cout<<binary_search(arr,arr+sizeof(arr)/sizeof(arr[0]),5)<<endl;

    vector<int> iV(arr+2,arr+5);

    cout<<*search(arr,arr+sizeof(arr)/sizeof(arr[0]),iV.begin(),iV.end())<<endl;

    return 0;
}
```

## 迭代器

- 是一种smart pointer,用于访问顺序容器和关联容器中的元素，相当于容器和操纵容器的算法之间的中介
- 迭代器按照定义方式
  - 正向迭代器`iterator`
  - 常量正向迭代器`const_iterator`
  - 反向迭代器`reverse_iterator`
  - 常量反向迭代器`const_reverse_iterator`

### 容器与迭代器

|容器|迭代器|
|:---:|:---|
|vector|随机访问|
|deque|随机访问|
|list|双向访问|
|set/multiset|双向访问|
|map/multimap|双向访问|
|stack|不支持迭代器|
|queue|不支持迭代器|
|priority_queue|不支持迭代器|

```C++
    priority_queue<int> pq;//默认最大值优先
    priority_queue<int,vector<int>,less<int>> pq2;//最大值优先
    priority_queue<int,vector<int>,greater<int>> pq3;//最小值优先
```

## 空间配置器（allocator）

## 多线程

- C++11中Thread的使用
- mutex等锁的使用
- 进程与线程，同步与异步
- 线程的交换与移动

# Linux C++

## Makefile基本规则

```make
目标(target):依赖(prerequisites)
    命令(command)
```

## Makefile简化规则

```make
变量定义：变量 = 字符串
变量使用：$(变量名)
```

## Makefile基本实例

不生成目标文件的命令最好都设置为假想目标

```make
.PHONY:clean
```

```make
TARGET = main
OBJS = main.o
.PHONY:clean
$(TARGET):$(OBJS)
    g++ $(OBJS) -o $(TARGET)
main.o:main.cpp
clean:
    rm $(TARGET) $(OBJS)
```
