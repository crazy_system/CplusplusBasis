/*
 * @Author: crazy_system 909395091@qq.com
 * @Date: 2023-12-27 10:44:50
 * @LastEditors: crazy_system 909395091@qq.com
 * @LastEditTime: 2023-12-28 10:44:40
 * @FilePath: /WorkSpace/CplusplusBasis/Basis/verctor.cpp
 */
#include<iostream>
#include<vector>
using namespace std;
void test1(){
    vector<int> vec = {1,2,4,5};
    vec.push_back(7);
    int c[4] = {0x80,0xFF,0x00,0x7F};
    int* a[4];//指针的数组
    int(*b)[4];//数组的指针
    b = &c;
    for(unsigned int i = 0;i<4;i++)
        a[i] = &(c[i]);
    cout<<*(a[1])<<endl;
    cout<<(*b)[2]<<endl;
}
void test2(){
    char ch = 'a';
    char* cp = &ch;
    //++,--操作
    char* cp2 = ++cp;
    char* cp3 = cp++;
    char* cp4 = --cp;
    char* cp5 = cp--;
    //++左值
    // ++cp2 = 97;
    // cp2++ = 97;

    //*++,++*
    *++cp2 = 98;
    char ch3 = *++cp2;
    *cp2++ = 98;
    char ch4 = *cp2++;

    //++++,----
    int a=1,b=2,c,d;
    c= a++ +b;
    char ch5 = ++*++cp;
}
void text3(){
    int a = 0;//(GVAR)全局初始化区
    int* p1;//(bss)全局未初始化区
    
    int b;//(stack)栈区变量
    char s[] = "abc";//(stack)栈区变量
    int* p2;//(stack)栈区变量
    char* p3 = "123456";//123456/0在常量区，p3在(stack)栈区
    static int c = 0;//(GVAR)全局(静态)初始化区
    p1 = new int(10);//(heap)堆区变量
    p1 = new int(10);//(heap)堆区变量
}
int main(){
    
    return 0;
}